import socketio

sio = socketio.Client(logger=True, engineio_logger=True)

@sio.event
def connect():
    print('connection established')

@sio.event
def my_message(data):
    print('message received with ', data)
    sio.emit('my response', {'response': 'my response'})

@sio.event
def connect_error(err):
    print("The connection failed!" , err )

@sio.event
def disconnect():
    print('disconnected from server')
sio.connect('http://localhost:5001')
sio.wait()
